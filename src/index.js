import React from 'react';
import ReactDOM from 'react-dom';
import Square from './Square';
import './index.scss';



class Board extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            /** player with note on every box */
            squares: Array(9).fill(Array(9).fill(null)),
            /** the 9 total fields */
            fields: Array(9).fill(null),
            disabledFields: Array(9).fill(false),
            xIsNext: true,
            nextField: null,
        };
    }

    handleClick(field, box) {
        const squares = this.state.squares.map((arr) => arr.slice());
        const fields = this.state.fields.slice();
        //catch existing elements and disabled fields
        if(squares[field][box] || this.state.disabledFields[field]) return;
        //get fields which are not playable and set the next playable field
        const disabledFields = this.state.disabledFields.fill(true);
        disabledFields[box] = false;
        //claim the field
        squares[field][box] = this.state.xIsNext ? 'X' : 'O';
        //determine if a field is won
        squares.forEach((square, field) => {
            console.log(square)
            console.log(fields[field])

            if(!fields[field] && calculateWinner(square)){
                fields[field] = this.state.xIsNext ? 'X' : 'O';
            }
        });
        //refresh the state
        this.setState({
            squares: squares,
            fields: fields,
            disabledFields: disabledFields,
            xIsNext: !this.state.xIsNext,
        });
    }
    //number = field*10+box
    renderSquare(field=0, box) {
        return (
            <Square
                box={box}
                field={field}
                disabled={this.state.disabledFields[field]}
                value={this.state.squares[field][box]}
                finished={this.state.fields[field]!==null}
                player={this.state.fields[field]==='X'}
                onClick={() => this.handleClick(field, box)}
            />
        );
    }

    render() {
        //calc if player ahs won
        const winner = calculateWinner(this.state.fields);
        let status;
        if (winner) {
            status = 'Winner: ' + winner;
        } else {
            status = 'Current player: ' + (this.state.xIsNext ? 'X' : 'O');
        }
        //set up the field in copying the first 3 fields
        let baseSquares= [[0, 1, 2,10,11,12,20,21,22], [3, 4, 5,13,14,15,23,24,25], [6, 7, 8,16,17,18,26,27,28]];
        let squares = [];
        for(let i=0; i<3; i++){
            for(let j = 0; j<3; j++) {
                let newsquares = baseSquares[j].concat()
                    .map(
                        x => (
                            x + i * 30));
                squares.push(newsquares);
            }
        }

        return (
            <div>
                <div className="status">{status}</div>
                {squares.map(
                    (row, index) => (
                        <div key={index} className="board-row">
                            {row.map((index) =>
                                this.renderSquare((index/10|0),index-((index/10|0)*10)))
                            }
                        </div>)
                )
                }
            </div>
        );
    }
}

class Game extends React.Component {
    render() {
        return (
            <div className="game">
                <div className="game-board">
                    <Board/>
                </div>
                <div className="game-info">
                    <div>{/* status */}</div>
                    <ol>{/* TODO */}</ol>
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <Game/>,
    document.getElementById('root')
);

function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }
    return null;
}
