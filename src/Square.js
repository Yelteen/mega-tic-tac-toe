import React from "react";


function determineSide(box, disabled, finished, player=null) {
    let result = "square ";
    if(box <= 2) result += "top ";
    if(box >= 6) result += "bottom ";
    if(box%3 === 0) result += "left ";
    if(box%3 ===2) result += "right ";
    if(disabled) result += "disabled ";
    if(finished){
        result += "finished ";
        if(player)
            result += "playera "
        else
            result+= "playerb "
    }

    return result;
}

export default class Square extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // the number of the box inside the field
            boxid: props.box,
            //the total field
            fieldid: props.field,
            key: props.field+'-'+props.box,
        }
    }

    render() {
        let side = determineSide(this.state.boxid, this.props.disabled, this.props.finished, this.props.player);
        return (
            <button key={this.state.key} className={side} onClick={this.props.onClick}>
                {this.props.value}
            </button>
        );
    }
}
