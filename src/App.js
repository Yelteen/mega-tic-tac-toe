export function App() {
    return <h1>Hello world!</h1>;
}

class Board extends React.Component {
    renderSquare(i) {
        return <Square value={i} />;  }
}

class Square extends React.Component {
    render() {
        return (
            <button className="square">
                {this.props.value}      </button>
        );
    }
}