# Meta Tic-Tac-Toe

This is a simple implementaion of the Meta Tic-Tac-Toe (also called Ultimate Tic-Tac-Toe), consisting of one large field and contining a small tic-tac-toe field each.

The rules are simple. The first player starts selecting on box on a field. The next player only can put it's marker on the large field, which the previous player has set its marker. So when one player sets its marker on the top left box on the field, the next player can sonly set it's marker on the top left box (of 9 fields). When one player has a line (like in the small version) thhis player wins the box. The player having a line of 3 boxes wins the game.

For further descirption see [Wikipedia](https://de.wikipedia.org/wiki/Meta-Tic-Tac-Toe)

## Installation
Besides the version on Gitlab Pages [https://yelteen.gitlab.io/mega-tic-tac-toe/](https://yelteen.gitlab.io/mega-tic-tac-toe/) You can set up your local version with yarn and parcel.js using ```yarn && yarn start```. The project itselfs bases on React.

## License
This project is licensed unter the MIT License.

## Project status
This project is just training for me to learn React, so there will be no active development. Feel free to clone it for your use.

